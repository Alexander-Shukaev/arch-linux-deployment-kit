### Preamble {{{
##  ==========================================================================
##        @file Makefile
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-07 Sunday 01:17:56 (+0200)
##  --------------------------------------------------------------------------
##     @created 2018-09-26 Wednesday 02:28:22 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Prefix {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
ifeq "$(wildcard PREFIX.mk)" ""
$(error PREFIX.mk: No such file or directory)
else
include PREFIX.mk
endif
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Prefix
##
### Header {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
include $(PREFIX)header.mk
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Header
##
### Body {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
include $(PREFIX)git.mk
include $(PREFIX)make.mk
include $(PREFIX)python.mk
include $(PREFIX)venv.mk
##  ==========================================================================
##  }}} Includes
##
### Variables {{{
##  ==========================================================================
override ALDK_PREPARE ?= $(CURDIR)/bin/aldk-prepare
override README_MD    ?= $(PWD)/README.md
##
override ALDK_PREPARE := $(call s/\s/\\s/,$(ALDK_PREPARE))
override README_MD    := $(call s/\s/\\s/,$(README_MD))
##
override define HINT :=
$(HRULE80)
make: In order to paginate the manual and prepare the installation, run the

  $(MAKE) prepare

command.
make: In order to set up the Python Virtual Environment, run the

  $(MAKE) venv

command.
$(HRULE80)
endef
##
# Default
override .DEFAULT_GOAL := all
##  ==========================================================================
##  }}} Variables
##
### Targets {{{
##  ==========================================================================
### All {{{
##  ==========================================================================
ifdef PYTHON
.PHONY: all
all: git make python venv readme
else
.PHONY: all
all: prepare
	@$(if $(call !expand,python),$(MAKE) $@)
endif
##  ==========================================================================
##  }}} All
##
### Hint {{{
##  ==========================================================================
.PHONY: hint
hint: | .silent
	@$(info $(HINT))
##  ==========================================================================
##  }}} Hint
##
### Help {{{
##  ==========================================================================
.PHONY: help
help:
	@$(ALDK_PREPARE) --help
	@$(MAKE) hint
##  ==========================================================================
##  }}} Help
##
### Manual {{{
##  ==========================================================================
.PHONY: man
man:
	@$(ALDK_PREPARE) --man
	@$(MAKE) hint
##
.PHONY: manual
manual: man
##  ==========================================================================
##  }}} Manual
##
### Prepare {{{
##  ==========================================================================
.PHONY: prepare
prepare:
	@$(ALDK_PREPARE)
	@$(MAKE) hint
##  ==========================================================================
##  }}} Prepare
##
### README {{{
##  ==========================================================================
.PHONY: readme
readme: $(README_MD)
##  ==========================================================================
##  }}} README
##
$(README_MD): $(MAKEFILE) $(ALDK_PREPARE)
	@$(if $(VERBOSE),printf "make: Writing file '%s'\n" '$@')
	@echo '```text'        >| '$@'
	@$(ALDK_PREPARE) --man >> '$@'
	@echo '```'            >> '$@'
##  ==========================================================================
##  }}} Targets
##  ==========================================================================
##  }}} Body
##
### Footer {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
include $(PREFIX)footer.mk
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Footer
