#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file .global.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-05-16 Wednesday 09:54:38 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-01-21 Thursday 18:46:59 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
  format_bold='\033[1m'
   format_red='\033[31m'
format_yellow='\033[33m'
format_normal='\033[0m'
##
stdout() (
  format="${1}"
  shift
  printf "${format}" "${@}"
)
##
stderr() {
  stdout "${@}" 1>&2
}
##
output() (
  format="${1}"
  shift
  stdout "${format}\n" "${@}"
)
##
error() (
  format="${1}"
  shift
  stderr "${format_error}: ${format}\n" 'error' "${@}"
)
format_error="${format_bold}${format_red}%s${format_normal}"
##
warning() (
  format="${1}"
  shift
  stdout "${format_warning}: ${format}\n" 'warning' "${@}"
)
format_warning="${format_bold}${format_yellow}%s${format_normal}"
##
die() {
  error "${@}"
  exit 1
}
##
pause() {
  read -r -s -n 1 -p 'Press any key to continue...'
  stdout '\n'
}
##
basename() {
  stdout '%s' "${1##*/}"
}
##
basename_sans_extension() (
  name="${1##*/}"
  stdout '%s' "${name%.*}"
)
##
which() {
  command which "${@}" 2> '/dev/null'
}
##
pager() (
  set +e
  pager="${PAGER:-$(basename "$(which 'less')")}"
  pager="${pager:-$(basename "$(which 'most')")}"
  pager="${pager:-$(basename "$(which 'more')")}"
  pager="${pager:-$(basename "$(which 'cat' )")}"
  stdout '%s' "${pager}"
)
##
require_root() {
  ((EUID != 0)) && die "Requires 'root' privileges"
  return 0
}
##
__file__() {
  stdout '%s' "$(readlink -f "${0}")"
}
##
__dir__() {
  stdout '%s' "$(dirname "$(__file__)")"
}
##
    ALDK_DIR="${ALDK_DIR:-$(dirname "$(__dir__)")}"
        ALDK="${ALDK:-${ALDK_DIR}/bin/aldk}"
ALDK_PREPARE="${ALDK:-${ALDK_DIR}/bin/aldk-prepare}"
