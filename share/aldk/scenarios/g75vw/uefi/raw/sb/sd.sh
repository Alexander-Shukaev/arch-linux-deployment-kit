#!/usr/bin/env sh
##
### Preamble {{{
##  ==========================================================================
##        @file sd.sh
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-05-14 Monday 16:25:00 (+0200)
##  --------------------------------------------------------------------------
##     @created 2015-12-13 Sunday 19:13:52 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
set -e
##
. "$(dirname "$(readlink -f "${0}")")/../../../../../.global.sh"
##
main() {
  "${ALDK_PREPARE}"
  partitions_file="${ALDK_DIR}/partitions/g75vw/uefi/raw/sb/sd.json"
  "${ALDK}" gpt delete                                                       \
    '/dev/sda'
  "${ALDK}" gpt create                                                       \
    "${partitions_file}"
  "${ALDK}" fs format                                                        \
    "${partitions_file}"
  "${ALDK}" fs mount                                                         \
    --prefix='/mnt'                                                          \
    "${partitions_file}"
  "${ALDK}" restore                                                          \
    --prefix='/mnt'                                                          \
    --verbose                                                                \
    "${1}"
  "${ALDK}" fs table add                                                     \
    --prefix='/mnt'                                                          \
    "${partitions_file}"
  arch-chroot                                                                \
    '/mnt'                                                                   \
    '/bin/sh'                                                                \
    -c                                                                       \
    "
      grub-install --target='x86_64-efi' --efi-directory='/boot/efi' --bootloader-id='grub' --recheck;
      grub-mkconfig -o '/boot/grub/grub.cfg';
      exit
    "
  "${ALDK}" fs unmount                                                       \
    --prefix='/mnt'                                                          \
    "${partitions_file}"
  pause
  reboot
}
##
main "${@}"
