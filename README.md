```text

                                      -`
                                     .o+`
                                    `ooo/
                                   `+oooo:
                                  `+oooooo:
                                  -+oooooo+:
                                `/:-:++oooo+:
                               `/++++/+++++++:
                              `/++++++++++++++:
                             `/+++ooooooooooooo/`
                            ./ooosssso++osssssso+`
                           .oossssso-````/ossssss+`
                          -osssssso.      :ssssssso.
                         :osssssss/        osssso+++.
                        /ossssssss/        +ssssooo/-
                      `/ossssso+/:-        -:/+osssso+-
                     `+sso+:-`                 `.-/+oso:
                    `++:.                           `-/+/
                     .`                                 `

                    << Arch Linux Deployment Kit (ALDK) >>
                       ================================

Description
===========

A simple CLI tool to automate tedious tasks of HDD partitioning, backup, and
deployment that allows for easy one-to-one migration of Arch Linux
installation across different hardware.

Configuration
=============

CPU Architecture: x86_64
Firmware Mode: UEFI

Guide
=====

GRUB
----

In order to successfully install GRUB on UEFI system, EFI System Partition
(ESP) is required.  Furthermore, booting the installation
environment/image/media (Arch Linux, Architect Linux, etc.) in UEFI (instead
of BIOS) firmware mode is absolutely mandatory.  [1, 2]

RAID
----

RAID systems have to be treated specially during boot.  In particular,
'initramfs' should contain the corresponding modules and hooks to be able to
handle RAID and recognize partitions on it.  Otherwise,

> ERROR: device 'UUID=<...>' not found. Skipping fsck.
> ERROR: Unable to find root device 'UUID=<...>'.
> <...>

would be issued on boot.

For instance, in case of (software) RAID0, the following configuration could
be put into '/etc/mkinitcpio.conf' [3]:

    MODULES="ext4 dm_mod raid0"
    HOOKS="base udev autodetect modconf block mdadm_udev filesystems"

------------------------------------------------------------------------------
> NOTE:
>
> Don't forget to rerun 'mkinitcpio -p linux[-lts]' to regenerate 'initramfs'.
------------------------------------------------------------------------------

LVM
---

When there are LVM volumes not activated (mounted) via the 'initramfs' (such
as LVM snapshots), it is necessary to enable the 'lvm2-monitor' service
because otherwise the system will hang for about 2 minutes upon shutdown with
the following messages in journal:

    lvm [<pid>]: There are still devices being monitored
    lvm [<pid>]: Refusing to exit

The solution is:

    # systemctl enable lvm2-monitor

When 'lvmetad' is used, LVM commands avoid scanning disks by reading metadata
from 'lvmetad'.  When new devices appear, they must be scanned so that their
metadata can be cached in 'lvmetad'.  This is done by the following command:

    # pvscan --cache

One typical scenario when one has to run this command is the following one.
Imagine that you have partitioned a device and created LVM physical volume
(PV) in one of the partitions.  Then you realized that you made a mistake
either in partitioning or wherever else, which requires you to reboot and
repeat the partitioning.  You do that and actually the new partitioning scheme
contains the same partition (with exactly the same offset and size as the one
where you previously created the LVM PV).  This time you want to create LVM PV
on that same partition once again, so you run (again)

    # pvcreate /dev/<dev-name>

and it fails because LVM still sees the metadata of the previously created PV
in the newly "overlayed" partition.  Thus, the solution is to run

    # pvscan --cache
    # pvremove -ff -y /dev/<dev-name>

and only then

    # pvcreate /dev/<dev-name>

will succeed.

LUKS
----

Format LUKS partition (device) with detached (remote) header:

    $ truncate -s 2M header
    # cryptsetup luksFormat --header header --verify-passphrase --align-payload 8192 --iter-time 3000 -c aes-xts-plain64 -s 512 -h sha512 /dev/<dev-name>

and open it either with:

    # cryptsetup luksOpen   --header header                                                                                               /dev/<dev-name> $(cryptsetup luksUUID --header header /dev/<dev-name>).luks

or with:

    # cryptsetup luksOpen   --header header                                                                                               /dev/<dev-name> $(blkid -s PARTUUID -o value          /dev/<dev-name>).luks

Finally, add key file to the key slot 1:

    $ dd bs=64 count=1 if=/dev/urandom of=1.key
    # cryptsetup luksAddKey --header header /dev/<dev-name> 1.key

------------------------------------------------------------------------------
> NOTE:
>
> Since the 512 bits master key is chosen for the LUKS device, a key file of
> 64 bytes (512 bits) is more than enough because, in general, a key that is
> larger than the master key of a LUKS device offers no additional security
> over a key that is the same size as the master key (as long as the key
> material is truly random).
------------------------------------------------------------------------------

tmpfs
-----

By default 'systemd' takes care about mounting 'tmpfs' at '/dev/shm', '/run'
(with '/var/lock' and '/var/run' being symbolic links to it for
compatibility), and '/tmp'.  Thus, no specific entries are required in
'/etc/fstab'.  [4]

VMware
------

In order to boot the installation environment/image/media (Arch Linux,
Architect Linux, etc.) in UEFI (instead of BIOS) firmware mode on VMware
virtual machine (VM), it is necessary to edit the '<vm-name>.vmx' (text) file
by adding the

    firmware = "efi"

line to it, where <vm-name> is the name of the corresponding VM.

In case of extra mouse buttons not functioning properly (clicks are merely
ignored) under VM, add

    mouse.vusb.enable        = "TRUE"
    mouse.vusb.useBasicMouse = "FALSE"
    usb.generic.allowHID     = "TRUE"

into the same '<vm-name>.vmx' file.

Block Devices (Partitions)
--------------------------

There are two important utilities with command-line interfaces which print
(possibly filtered) information about available block devices (partitions) on
the host system:

    $ lsblk -h

and

    $ blkid -h

UEFI Bootable UFD with Architect Linux
--------------------------------------

1.  Unmount UFD (USB Flash Drive) if it is mounted.

        # umount /dev/<dev-name>

2.  Format UFD with the FAT32 file system (FS) by issuing either

        # mkdosfs -F 32 -I /dev/<dev-name>

    or

        # mkfs.vfat /dev/<dev-name>

3.  Label the (just formatted) UFD:

        # dosfslabel /dev/<dev-name> architect

4.  Download the (latest) Architect Linux ISO image [6] and copy (exactly) all
    of the directories and files in it to the UFD.

5.  Boot from the UFD.

6.  Press [<e>] or [<TAB>] to edit the kernel parameters and add

        cow_spacesize=4G

    anywhere on the kernel command line.

    --------------------------------------------------------------------------
    > NOTE:
    >
    > Another advertised method (once already booted)
    >
    >     # mount -o remount,size=4G /run/archiso/cowspace
    >
    > does not seem to work properly.  That is although
    >
    >     # df -h
    >
    > would display the 'cowspace' file system (FS) with the size of 4G, one
    > would still continue to run into out of space errors (e.g. upon package
    > installations).
    --------------------------------------------------------------------------

7.  Most likely, one will face:

    > Mounting '/dev/disk/by-label/architect-201601-64bit' to '/run/archiso/bootmnt'
    > Waiting 30 seconds for device /dev/disk/by-label/architect-201601-64bit ...
    > ERROR: '/dev/disk/by-label/architect-201601-64bit' device did not show up after 30 seconds...
    >    Falling back to interactive prompt
    >    You can try to fix the problem manually, log out when you are finished
    > sh: can't access tty; job control turned off

    The solution is simple:

        # cd /dev/disk/by-label
        # ln -s architect architect-201601-64bit
        # exit

Issues
------

-   The 'failed unmounting /usr' error on shutdown can be safely ignored.
-   The 'failed unmounting /var' error on shutdown can be safely ignored.

Partitions
----------

Create a custom '<partitions-file>' JSON file outlining the target
partitioning scheme (see examples in the 'share/aldk/partitions' directory).

------------------------------------------------------------------------------
> NOTE:
>
> Before actually running any modifying ALDK command(s) and applying any
> (potentially destructive) changes to a system, it is strongly recommended to
> perform dry run(s) first, e.g.
>
>     $ aldk -n gpt create <partitions-file>
>
> and
>
>     $ aldk -n fs format <partitions-file>
>
> and so on, what causes ALDK to print the commands that are needed to fulfill
> the target (sub)commands (i.e. 'gpt' and 'fs' in the above examples), but
> not actually execute them.  It is a good practice to analyze, debug, and
> double-check the application consequences of both custom '<partitions-file>'
> JSON files and planned changes this way.  For example,
>
>     $ aldk -n gpt create g75vw/uefi/lvm/md.json
>
> produces
>
>     aldk: Using partitions file: 'g75vw/uefi/lvm/md.json'
>     aldk> /bin/sgdisk -n 2:8388608:67108863 /dev/md126
>     aldk> /bin/sgdisk -c '2:Swap PV' /dev/md126
>     aldk> /bin/sgdisk -t 2:8E00 /dev/md126
>     aldk> /bin/sgdisk -n 3:67108864:201326591 /dev/md126
>     aldk> /bin/sgdisk -c '3:Arch Linux PV' /dev/md126
>     aldk> /bin/sgdisk -t 3:8E00 /dev/md126
>     aldk> /bin/sgdisk -n 1:4194304:6291455 /dev/md126
>     aldk> /bin/sgdisk -c 1:EFI /dev/md126
>     aldk> /bin/sgdisk -t 1:EF00 /dev/md126
>     aldk> /bin/sgdisk -n 4:671088640:805306367 /dev/md126
>     aldk> /bin/sgdisk -c 4:Backup /dev/md126
>     aldk> /bin/sgdisk -t 4:8300 /dev/md126
>     aldk> /bin/sgdisk -n 5:838860800:1912602623 /dev/md126
>     aldk> /bin/sgdisk -c '5:Data PV' /dev/md126
>     aldk> /bin/sgdisk -t 5:8E00 /dev/md126
>
> and
>
>     $ aldk -n fs format g75vw/uefi/lvm/md.json
>
> produces
>
>     aldk: Using partitions file: 'g75vw/uefi/lvm/md.json'
>     aldk> /bin/mkfs.vfat -F 32 /dev/md126p1
>     aldk> /bin/mkfs.ext4 /dev/md126p4
------------------------------------------------------------------------------

Instructions
------------

# aldk gpt delete               /dev/<dev-name>
# aldk gpt create               <partitions-file>
# aldk fs  format               <partitions-file>
# aldk fs  mount  --prefix=/mnt <partitions-file>
<...>
Deploy Arch Linux with your favorite method; possibilities include
1.  Pacstrap [5]:

    # pacstrap -i /mnt base base-devel

2.  Architect Linux [6]:

    # /initialise

3.  Arch Linux Deployment Kit (ALDK) [7]:

    # aldk deploy --prefix=/mnt --verbose <backup-file>
<...>
# aldk grub uefi install --prefix=/mnt [--esp]
# aldk fs table   add --prefix=/mnt -L <partitions-file>
# aldk fs unmount     --prefix=/mnt    <partitions-file>

Scheduling
----------

In order to schedule some command to run at certain time (in future), e.g.
'hibernate', run:

$ echo 'systemctl hibernate' | sudo at now + 45 min

Examples
========

LVM on GPT, UEFI, and RAID0
---------------------------

# pacman -Syu
# pacman -S archlinux-keyring lzip
# aldk gpt delete                                                         /dev/md126
# aldk gpt create                                                         <partitions-file>
# aldk fs  format                                                         <partitions-file>
# pvcreate                                                                /dev/md126p2
# pvcreate                                                                /dev/md126p3
# pvcreate                                                                /dev/md126p5
# vgcreate                                       ${HOSTNAME}.swap.vg      /dev/md126p2
# vgcreate                                       ${HOSTNAME}.archlinux.vg /dev/md126p3
# vgcreate                                       ${HOSTNAME}.data.vg      /dev/md126p5
# lvcreate -C y -L 24G  -n swap.lv               ${HOSTNAME}.swap.vg      /dev/md126p2
# lvcreate -C y -L 4G   -n root.lv               ${HOSTNAME}.archlinux.vg /dev/md126p3
# lvcreate -C y -L 32G  -n usr.lv                ${HOSTNAME}.archlinux.vg /dev/md126p3
# lvcreate -C y -L 24G  -n var.lv                ${HOSTNAME}.archlinux.vg /dev/md126p3
# lvcreate -C y -L 256M -n applications.lv       ${HOSTNAME}.archlinux.vg /dev/md126p3
# lvcreate -C y -L 256M -n icons.lv              ${HOSTNAME}.archlinux.vg /dev/md126p3
# lvcreate -C y -L 256M -n mime.lv               ${HOSTNAME}.archlinux.vg /dev/md126p3
# lvcreate -C y -L 256M -n local.applications.lv ${HOSTNAME}.archlinux.vg /dev/md126p3
# lvcreate -C y -L 256M -n local.icons.lv        ${HOSTNAME}.archlinux.vg /dev/md126p3
# lvcreate -C y -L 256M -n local.mime.lv         ${HOSTNAME}.archlinux.vg /dev/md126p3
# lvcreate -C y -L 256G -n home.lv               ${HOSTNAME}.data.vg      /dev/md126p5
# lvdisplay | less
# mkswap                         /dev/${HOSTNAME}.swap.vg/swap.lv
# swapon                         /dev/${HOSTNAME}.swap.vg/swap.lv
# mkfs.ext4 -L 'AL'              /dev/${HOSTNAME}.archlinux.vg/root.lv
# mkfs.ext4 -L 'AL/Usr'          /dev/${HOSTNAME}.archlinux.vg/usr.lv
# mkfs.ext4 -L 'AL/Var'          /dev/${HOSTNAME}.archlinux.vg/var.lv
# mkfs.ext4 -L 'AL/Applications' /dev/${HOSTNAME}.archlinux.vg/applications.lv
# mkfs.ext4 -L 'AL/Icons'        /dev/${HOSTNAME}.archlinux.vg/icons.lv
# mkfs.ext4 -L 'AL/MIME'         /dev/${HOSTNAME}.archlinux.vg/mime.lv
# mkfs.ext4 -L 'L/Applications'  /dev/${HOSTNAME}.archlinux.vg/local.applications.lv
# mkfs.ext4 -L 'L/Icons'         /dev/${HOSTNAME}.archlinux.vg/local.icons.lv
# mkfs.ext4 -L 'L/MIME'          /dev/${HOSTNAME}.archlinux.vg/local.mime.lv
# mkfs.ext4 -L 'Data/Home'       /dev/${HOSTNAME}.data.vg/home.lv
# mkdir -p                                                      /mnt/boot/efi
# mkdir -p                                                      /mnt/usr
# mkdir -p                                                      /mnt/var
# mkdir -p                                                      /mnt/home
# mount     /dev/${HOSTNAME}.archlinux.vg/root.lv               /mnt
# mount     /dev/md126p1                                        /mnt/boot/efi
# mount     /dev/${HOSTNAME}.archlinux.vg/usr.lv                /mnt/usr
# mount     /dev/${HOSTNAME}.archlinux.vg/var.lv                /mnt/var
# mount     /dev/${HOSTNAME}.archlinux.vg/applications.lv       /mnt/usr/share/applications
# mount     /dev/${HOSTNAME}.archlinux.vg/icons.lv              /mnt/usr/share/icons
# mount     /dev/${HOSTNAME}.archlinux.vg/mime.lv               /mnt/usr/share/mime
# mount     /dev/${HOSTNAME}.archlinux.vg/local.applications.lv /mnt/usr/local/share/applications
# mount     /dev/${HOSTNAME}.archlinux.vg/local.icons.lv        /mnt/usr/local/share/icons
# mount     /dev/${HOSTNAME}.archlinux.vg/local.mime.lv         /mnt/usr/local/share/mime
# mount     /dev/${HOSTNAME}.data.vg/home.lv                    /mnt/home
# LZIP=-d aldk deploy --prefix=/mnt --decompressor=lzip --verbose ${HOSTNAME}-DD.MM.YYYY-I.backup.tar.lz
# genfstab -L /mnt > /mnt/etc/fstab
# arch-chroot /mnt
# aldk grub uefi install --prefix=/mnt --default

------------------------------------------------------------------------------
> NOTE:
>
> It is possible to perform online/live grow/extend of the whole storage stack
> in the bottom-up manner: GPT partition, LVM PV, LVM LV, FS.  That is (in
> application for the above example of the storage stack configuration) for
> instance:
>
>     # sgdisk    -d 3 -n 3:67108864:335544319 -c 3:'Arch Linux PV' -u 3:f1f35ef2-3a47-417b-9203-9d731a823443 -t 3:8E00 /dev/md126
>     # partprobe -s /dev/md126
>     # pvresize  /dev/md126p3
>     # lvchange  --alloc inherit /dev/${HOSTNAME}.archlinux.vg/usr.lv
>     # lvchange  --alloc inherit /dev/${HOSTNAME}.archlinux.vg/var.lv
>     # lvextend  -L64G           /dev/${HOSTNAME}.archlinux.vg/usr.lv
>     # lvextend  -L48G           /dev/${HOSTNAME}.archlinux.vg/var.lv
>     # resize2fs                 /dev/${HOSTNAME}.archlinux.vg/usr.lv
>     # resize2fs                 /dev/${HOSTNAME}.archlinux.vg/var.lv
------------------------------------------------------------------------------

LVM on GPT, LUKS on LVM, and LVM on LUKS
----------------------------------------

# export MODEL=st9250315as
# export    LV=user
# shred --verbose --random-source=/dev/urandom -n1          /dev/<dev-name>
# aldk gpt create                                           <partitions-file>
# aldk fs  format                                           <partitions-file>
# pvcreate                                                  /dev/<dev-name>2
# vgcreate                              ${MODEL}.storage.vg /dev/<dev-name>2
# lvcreate      -l 100%FREE -n ${LV}.lv ${MODEL}.storage.vg
# truncate -s 2M header
# cryptsetup luksFormat --header header --verify-passphrase --align-payload 8192 --iter-time 3000 -c aes-xts-plain64 -s 512 -h sha512 /dev/${MODEL}.storage.vg/${LV}.lv
# cryptsetup luksOpen   --header header                                                                                               /dev/${MODEL}.storage.vg/${LV}.lv ${MODEL}.storage.vg-${LV}.lv.luks
# dd bs=64 count=1 if=/dev/urandom of=1.key
# cryptsetup luksAddKey --header header                                                                                               /dev/${MODEL}.storage.vg/${LV}.lv 1.key
# pvcreate                                                   /dev/mapper/${MODEL}.storage.vg-${LV}.lv.luks
# vgcreate                                 ${MODEL}.${LV}.vg /dev/mapper/${MODEL}.storage.vg-${LV}.lv.luks
# lvcreate -C y -L 24G      -n backup.lv   ${MODEL}.${LV}.vg /dev/mapper/${MODEL}.storage.vg-${LV}.lv.luks
# lvcreate -C y -L 8G       -n projects.lv ${MODEL}.${LV}.vg /dev/mapper/${MODEL}.storage.vg-${LV}.lv.luks
# lvcreate -C y -L 24G      -n pictures.lv ${MODEL}.${LV}.vg /dev/mapper/${MODEL}.storage.vg-${LV}.lv.luks
# lvcreate -C y -L 48G      -n videos.lv   ${MODEL}.${LV}.vg /dev/mapper/${MODEL}.storage.vg-${LV}.lv.luks
# lvcreate -C y -L 96G      -n music.lv    ${MODEL}.${LV}.vg /dev/mapper/${MODEL}.storage.vg-${LV}.lv.luks
# lvcreate -C y -L 16G      -n software.lv ${MODEL}.${LV}.vg /dev/mapper/${MODEL}.storage.vg-${LV}.lv.luks
# lvcreate -C y -L 8G       -n stash.lv    ${MODEL}.${LV}.vg /dev/mapper/${MODEL}.storage.vg-${LV}.lv.luks
# e2label /dev/<dev-name>1 'User/Boot'
# mkfs.ext4 -L 'User/Backup'   /dev/${MODEL}.${LV}.vg/backup.lv
# mkfs.ext4 -L 'User/Projects' /dev/${MODEL}.${LV}.vg/projects.lv
# mkfs.ext4 -L 'User/Pictures' /dev/${MODEL}.${LV}.vg/pictures.lv
# mkfs.ext4 -L 'User/Videos'   /dev/${MODEL}.${LV}.vg/videos.lv
# mkfs.ext4 -L 'User/Music'    /dev/${MODEL}.${LV}.vg/music.lv
# mkfs.ext4 -L 'User/Software' /dev/${MODEL}.${LV}.vg/software.lv
# mkfs.ext4 -L 'User/Stash'    /dev/${MODEL}.${LV}.vg/stash.lv

References
==========

[1] <http://wiki.archlinux.org/index.php/GRUB>
[2] <http://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface>
[3] <http://wiki.archlinux.org/index.php/mkinitcpio>
[4] <http://wiki.archlinux.org/index.php/tmpfs>
[5] <http://wiki.archlinux.org/index.php/Beginners'_guide>
[6] <http://sourceforge.net/projects/architect-linux>
[7] <http://bitbucket.org/Alexander-Shukaev/arch-linux-deployment-kit>

Help
====

Usage:
  aldk [options] gpt create <device> <number> <offset> <size> <name> <type>
  aldk [options] gpt create [--indexes=<indexes>] <partitions-file>
  aldk [options] gpt delete <device> [<number>]
  aldk [options] gpt delete [--indexes=<indexes>] <partitions-file>
  aldk [options] fs format <device> <number> <fs>
  aldk [options] fs format [--indexes=<indexes>] <partitions-file>
  aldk [options] fs mount [--options=<options>] <source> <dir>
  aldk [options] fs mount [--prefix=<prefix>] [--indexes=<indexes>]
                          <partitions-file>
  aldk [options] fs unmount <dir>
  aldk [options] fs unmount [--prefix=<prefix>] [--indexes=<indexes>]
                            <partitions-file>
  aldk [options] fs swap on <device> <number>
  aldk [options] fs swap on [--indexes=<indexes>] <partitions-file>
  aldk [options] fs swap off <device> <number>
  aldk [options] fs swap off [--indexes=<indexes>] <partitions-file>
  aldk [options] fs table add [--prefix=<prefix>] [--options=<options>]
                              <source> <dir> <fs> <dump> <pass>
  aldk [options] fs table add [--prefix=<prefix>] [--indexes=<indexes>]
                              [--label | --uuid] <partitions-file>
  aldk [options] grub bios install [--prefix=<prefix>] <device>
  aldk [options] grub uefi install [--prefix=<prefix>] [--default] [--esp]
                                   [--i386]
  aldk [options] backup [--prefix=<prefix>] [--compressor=<program>]
                        [--excludes-file=<file>] [--backup-dir=<dir>]
                        [--verbose]
  aldk [options] deploy [--prefix=<prefix>] [--decompressor=<program>]
                        [--verbose] <backup-file>
  aldk (-h | --help)
  aldk (-V | --version)

Options:
  -L, --label
    Use labels for source identifiers.

  -U, --uuid
    Use UUIDs for source identifiers.

  -b, --backup-dir=<dir>
    Specify directory to store backups [default: /var/tmp/backup].

  -c, --compressor=<program>
    Specify program to perform compression.

  -d, --decompressor=<program>
    Specify program to perform decompression.

      --default
    Install GRUB as default bootloader on UEFI system.

      --esp
    Install GRUB into EFI System Partition (ESP).

  -e, --excludes-file=<file>
    Specify file containing exclude patterns [default: default.json].

  -h, --help
    Print this help message.

      --i386
    Install GRUB for 32-bit UEFI system.

  -i, --indexes=<indexes>
    Specify comma-separated list of indexes.

  -n, --just-print, --dry-run, --recon
    Don't actually run any command; just print them.

  -o, --options=<options>
    Specify comma-separated list of options.

  -p, --prefix=<prefix>
    Specify prefix [default: /].

  -v, --verbose
    Print more details.

  -V, --version
    Print version.

```
