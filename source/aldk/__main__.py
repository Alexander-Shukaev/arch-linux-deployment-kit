#!/usr/bin/env python3
# encoding: utf-8
##
### Preamble {{{
##  ==========================================================================
##        @file __main__.py
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-09-30 Sunday 15:59:49 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-01-21 Thursday 19:06:47 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Imports {{{
##  ==========================================================================
from __future__ import absolute_import
##
import os
import sys
##  ==========================================================================
##  }}} Imports
##
### Constants {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
##  ==========================================================================
##  }}} Public
##
### Private {{{
##  ==========================================================================
__realfile__ = os.path.realpath(__file__)
__dir__      = os.path.dirname(__file__)
__realdir__  = os.path.dirname(__realfile__)
##  ==========================================================================
##  }}} Private
##  ==========================================================================
##  }}} Constants
##
### Main {{{
##  ==========================================================================
if __name__ == '__main__':
  if __package__ == '':
    path = os.path.normpath(sys.path[0])
    if os.path.isabs(path):
      sys.path[0], __package__ = os.path.split(path)
    else:
      sys.path[0], __package__ = '', '.'.join(path.split(os.path.sep))
    sys.path.insert(0, os.path.dirname(__realdir__))
    from aldk import main
  else:
    path = os.path.realpath(os.path.sep.join(__package__.split('.')))
    print("Running ({0}) module '{1}' as a script from file '{2}'"
          .format('relative' if path == __realdir__ else 'absolute',
                  __package__,
                  __realfile__))
    assert sys.path[0] == ''
    from . import main
  sys.exit(main())
##  ==========================================================================
##  }}} Main
