#!/usr/bin/env python3
# encoding: utf-8
##
### Preamble {{{
##  ==========================================================================
##        @file __init__.py
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2021-06-21 Monday 00:06:56 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-01-21 Thursday 19:06:47 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2021,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Docstring {{{
##  ==========================================================================
"""
Usage:
  aldk [options] gpt create <device> <number> <offset> <size> <name> <type>
  aldk [options] gpt create [--indexes=<indexes>] <partitions-file>
  aldk [options] gpt delete <device> [<number>]
  aldk [options] gpt delete [--indexes=<indexes>] <partitions-file>
  aldk [options] fs format <device> <number> <fs>
  aldk [options] fs format [--indexes=<indexes>] <partitions-file>
  aldk [options] fs mount [--options=<options>] <source> <dir>
  aldk [options] fs mount [--prefix=<prefix>] [--indexes=<indexes>]
                          <partitions-file>
  aldk [options] fs unmount <dir>
  aldk [options] fs unmount [--prefix=<prefix>] [--indexes=<indexes>]
                            <partitions-file>
  aldk [options] fs swap on <device> <number>
  aldk [options] fs swap on [--indexes=<indexes>] <partitions-file>
  aldk [options] fs swap off <device> <number>
  aldk [options] fs swap off [--indexes=<indexes>] <partitions-file>
  aldk [options] fs table add [--prefix=<prefix>] [--options=<options>]
                              <source> <dir> <fs> <dump> <pass>
  aldk [options] fs table add [--prefix=<prefix>] [--indexes=<indexes>]
                              [--label | --uuid] <partitions-file>
  aldk [options] grub bios install [--prefix=<prefix>] <device>
  aldk [options] grub uefi install [--prefix=<prefix>] [--default] [--esp]
                                   [--i386]
  aldk [options] backup [--prefix=<prefix>] [--compressor=<program>]
                        [--excludes-file=<file>] [--backup-dir=<dir>]
                        [--verbose]
  aldk [options] deploy [--prefix=<prefix>] [--decompressor=<program>]
                        [--verbose] <backup-file>
  aldk (-h | --help)
  aldk (-V | --version)

Options:
  -L, --label
    Use labels for source identifiers.

  -U, --uuid
    Use UUIDs for source identifiers.

  -b, --backup-dir=<dir>
    Specify directory to store backups [default: /var/tmp/backup].

  -c, --compressor=<program>
    Specify program to perform compression.

  -d, --decompressor=<program>
    Specify program to perform decompression.

      --default
    Install GRUB as default bootloader on UEFI system.

      --esp
    Install GRUB into EFI System Partition (ESP).

  -e, --excludes-file=<file>
    Specify file containing exclude patterns [default: default.json].

  -h, --help
    Print this help message.

      --i386
    Install GRUB for 32-bit UEFI system.

  -i, --indexes=<indexes>
    Specify comma-separated list of indexes.

  -n, --just-print, --dry-run, --recon
    Don't actually run any command; just print them.

  -o, --options=<options>
    Specify comma-separated list of options.

  -p, --prefix=<prefix>
    Specify prefix [default: /].

  -v, --verbose
    Print more details.

  -V, --version
    Print version.

"""
##  ==========================================================================
##  }}} Docstring
##
### Imports {{{
##  ==========================================================================
from __future__ import absolute_import
##
import errno
import os
import site
import sys
##
from schema import And
from schema import Optional
from schema import Or
from schema import Schema
from schema import Use
##
try:
  from .__version import version as __version__
except ImportError:
  __version__ = 'unknown'
##  ==========================================================================
##  }}} Imports
##
### Constants {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
__all__ = [
  '__version__',
  'main',
]
##  ==========================================================================
##  }}} Public
##
### Private {{{
##  ==========================================================================
NAME            = __package__.split('.')[-1]
SHARE_PATH      = [path
                   for path in [os.path.join(prefix, 'share', NAME)
                                for prefix in site.PREFIXES]
                   if os.path.isdir(path)]
EXCLUDES_PATH   = [os.path.join(path, 'excludes'  ) for path in SHARE_PATH]
PARTITIONS_PATH = [os.path.join(path, 'partitions') for path in SHARE_PATH]
##
JUST_PRINT      = False
##  ==========================================================================
##  }}} Private
##  ==========================================================================
##  }}} Constants
##
### Functions {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
def main(argv=None):
  from docopt import docopt
  name = 'Arch Linux Deployment Kit (ALDK)'
  args = docopt(__doc__, argv=argv, version='{} {}'.format(name, __version__))
  ##
  if ('--just-print' in args and args['--just-print']) or                    \
     ('--dry-run'    in args and args['--dry-run']) or                       \
     ('--recon'      in args and args['--recon']):
    global JUST_PRINT
    JUST_PRINT = True
  ##
  require_root()
  ##
  def filter_dict(d, *keys):
    return {k: d[k] for k in keys if d[k] is not None}
  ##
  if args['create']:
    args = filter_dict(args,
                       '<device>',
                       '<number>',
                       '<offset>',
                       '<size>',
                       '<name>',
                       '<type>',
                       '--indexes',
                       '<partitions-file>')
    gpt_create(**args)
  elif args['delete']:
    args = filter_dict(args,
                       '<device>',
                       '<number>',
                       '--indexes',
                       '<partitions-file>')
    gpt_delete(**args)
  elif args['format']:
    args = filter_dict(args,
                       '<device>',
                       '<number>',
                       '<fs>',
                       '--indexes',
                       '<partitions-file>')
    fs_format(**args)
  elif args['mount']:
    args = filter_dict(args,
                       '--options',
                       '<source>',
                       '<dir>',
                       '--prefix',
                       '--indexes',
                       '<partitions-file>')
    fs_mount(**args)
  elif args['unmount']:
    args = filter_dict(args,
                       '<dir>',
                       '--prefix',
                       '--indexes',
                       '<partitions-file>')
    fs_unmount(**args)
  elif args['swap']:
    if args['on']:
      args = filter_dict(args,
                         '<device>',
                         '<number>',
                         '--indexes',
                         '<partitions-file>')
      fs_swap_on(**args)
  elif args['swap']:
    if args['off']:
      args = filter_dict(args,
                         '<device>',
                         '<number>',
                         '--indexes',
                         '<partitions-file>')
      fs_swap_off(**args)
  elif args['table']:
    if args['add']:
      args = filter_dict(args,
                         '--prefix',
                         '--options',
                         '<source>',
                         '<dir>',
                         '<fs>',
                         '<dump>',
                         '<pass>',
                         '--indexes',
                         '<partitions-file>')
      fs_table_add(**args)
  elif args['grub'] and args['bios'] and args['install']:
    args = filter_dict(args,
                       '--prefix',
                       '<device>')
    grub_bios_install(**args)
  elif args['grub'] and args['uefi'] and args['install']:
    args = filter_dict(args,
                       '--prefix',
                       '--default',
                       '--esp',
                       '--i386')
    grub_uefi_install(**args)
  elif args['backup']:
    args = filter_dict(args,
                       '--prefix',
                       '--compressor',
                       '--excludes-file',
                       '--backup-dir',
                       '--verbose')
    backup(**args)
  elif args['deploy']:
    args = filter_dict(args,
                       '--prefix',
                       '--decompressor',
                       '--verbose',
                       '<backup-file>')
    deploy(**args)
  else:
    # Dead (unreachable) code:
    pass
  return 0
##  ==========================================================================
##  }}} Public
##
### Private {{{
##  ==========================================================================
def just_print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False):
  prefixed_objects = [NAME + '>']
  prefixed_objects.extend(objects)
  print(*prefixed_objects, sep=sep, end=end, file=file, flush=flush)
##
def message(*objects, sep=' ', end='\n', file=sys.stdout, flush=False):
  prefixed_objects = [NAME + ':']
  prefixed_objects.extend(objects)
  print(*prefixed_objects, sep=sep, end=end, file=file, flush=flush)
##
def warning(*objects, sep=' ', end='\n', file=sys.stderr, flush=False):
  prefixed_objects = [NAME + ':', 'warning:']
  prefixed_objects.extend(objects)
  print(*prefixed_objects, sep=sep, end=end, file=file, flush=flush)
##
def prefix(path, *prefixes):
  if os.path.isabs(path) or path.split(os.path.sep)[0] in {os.path.curdir,
                                                           os.path.pardir}:
    return [os.path.abspath(path)]
  return [os.path.abspath(os.path.join(prefix, path)) for prefix in prefixes]
##
def open_file(path, category, *prefixes, **kwargs):
  error = IOError(errno.ENOENT, os.strerror(errno.ENOENT), path)
  path  = prefix(path, *prefixes)
  for p in path:
    try:
      f = open(p, **kwargs)
    except IOError as e:
      error = e
      pass
    else:
      message("Using {} file: '{}'".format(category.lower(), p))
      return f
  raise error
##
def open_excludes_file(path, **kwargs):
  return open_file(path, 'excludes', *EXCLUDES_PATH, **kwargs)
##
def open_partitions_file(path, **kwargs):
  return open_file(path, 'partitions', *PARTITIONS_PATH, **kwargs)
##
def quote_args(*args):
  from re import search
  args = list(args)
  for i, arg in enumerate(args):
    args[i] = ("'{}'".format(arg.replace("'", "'\\''"))
               if search(r'\s', arg) else arg.replace("'", "\\'"))
  return args
##
def call(args, **kwargs):
  from subprocess import CompletedProcess
  from subprocess import run
  if JUST_PRINT:
    def to_strings(env):
      strings = []
      if env:
        for k, v in env.items():
          strings.extend("{}='{}'".format(k, '' if v is None else v))
      return strings
    ##
    just_print(*to_strings(kwargs.get('env')), *quote_args(*args))
    return CompletedProcess(args=list(args), returncode=0)
  return run(args, **kwargs, check=True)
##
def user_id():
  from subprocess import CalledProcessError
  from subprocess import PIPE
  from subprocess import Popen
  args = ('/bin/id', '-u')
  p = Popen(args, stdout=PIPE)
  user_id = int(p.communicate()[0])
  if p.returncode != 0:
    raise CalledProcessError(p.returncode, ' '.join(args))
  return user_id
##
def is_root():
  return user_id() == 0
##
def require_root():
  if JUST_PRINT:
    return
  if not is_root():
    raise PermissionError("has to be run with 'root' privileges")
##
def require_not_exists(file):
  if JUST_PRINT:
    return
  if os.path.exists(file):
    raise OSError(errno.EEXIST, os.strerror(errno.EEXIST), file)
##
def check_grub_install():
  file = '/usr/bin/grub-install'
  if os.path.exists(file):
    return
  warning("Missing executable file: '{}'".format(file))
  message("In order to install, run",
          "",
          "  pacman -S grub",
          "",
          "command.",
          sep='\n')
##
def check_grub_mkconfig():
  file = '/usr/bin/grub-mkconfig'
  if os.path.exists(file):
    return
  warning("Missing executable file: '{}'".format(file))
  message("In order to install, run",
          "",
          "  pacman -S grub",
          "",
          "command.",
          sep='\n')
##
def check_intel_ucode():
  file = '/boot/intel-ucode.img'
  if os.path.exists(file):
    return
  warning("Missing image file: '{}'".format(file))
  message("In order to install, run",
          "",
          "  pacman -S intel-ucode",
          "",
          "command.",
          sep='\n')
##
def check_os_prober():
  # To have 'grub-mkconfig' search for other installed operating systems (OS),
  # recommend to install 'os-prober':
  file = '/usr/bin/os-prober'
  if os.path.exists(file):
    return
  warning("Missing executable file: '{}'".format(file))
  message("In order to install, run",
          "",
          "  pacman -S os-prober",
          "",
          "command.",
          sep='\n')
##
def blkid(tag, device):
  from subprocess import CalledProcessError
  from subprocess import PIPE
  from subprocess import Popen
  args = ('blkid', '-s', tag, device)
  p = Popen(args, stdout=PIPE)
  s = p.communicate()[0]
  if p.returncode != 0:
    raise CalledProcessError(p.returncode, ' '.join(args))
  return s
##
def sector(literal):
  from re import match
  m = match(r'(?P<quantity>\d+)(?P<unit>[kmgtp]?)', literal.strip().lower())
  if not m:
    raise ValueError("invalid literal for sector(): '{}'".format(literal))
  q = int(m.group('quantity'))
  u = m.group('unit')
  s = {
    '':  q << 0,
    'k': q << 1,
    'm': q << 11,
    'g': q << 21,
    't': q << 31,
    'p': q << 41,
  }.get(u, -1)
  if s < 0:
    raise ValueError("invalid literal for sector(): '{}'".format(literal))
  return s
##
def partitions(**args):
  partitions_file = Schema(
    Use(open_partitions_file)
  ).validate(args['<partitions-file>'])
  from json import load
  partitions = load(partitions_file)
  if '--indexes' in args:
    indexes = Schema(
      And(str,
          len,
          Use(lambda s: list(map(int, s.split(',')))))
    ).validate(args['--indexes'])
    sorted_partitions = []
    for index in indexes:
      sorted_partitions.append(partitions[index])
    partitions = sorted_partitions
  return partitions
##
def gpt_create(**args):
  if '<device>' in args and                                                  \
     '<number>' in args and                                                  \
     '<offset>' in args and                                                  \
     '<size>'   in args and                                                  \
     '<name>'   in args and                                                  \
     '<type>'   in args:
    device = Schema(
      And(Use(os.path.normpath),
          os.path.exists)
    ).validate(args['<device>'])
    number = Schema(
      And(Use(int),
          lambda i: i >= 0)
    ).validate(args['<number>'])
    start = Schema(
      Use(sector)
    ).validate(args['<offset>'])
    size = Schema(
      Use(sector)
    ).validate(args['<size>'])
    name = Schema(
      And(str, len)
    ).validate(args['<name>'])
    from string import hexdigits
    type_ = Schema(
      lambda s: all(c in hexdigits for c in s)
    ).validate(args['<type>'])
    stop = start + size - 1
    call(('/bin/sgdisk',
          '-n', '{}:{}:{}'.format(number, start, stop),
          device))
    call(('/bin/sgdisk',
          '-c', '{}:{}'.format(number, name),
          device))
    call(('/bin/sgdisk',
          '-t', '{}:{}'.format(number, type_),
          device))
  else:
    for partition in partitions(**args):
      if 'protected' in partition and partition['protected']:
        continue
      if 'device' not in partition or                                        \
         'number' not in partition or                                        \
         'offset' not in partition or                                        \
         'size'   not in partition or                                        \
         'name'   not in partition or                                        \
         'type'   not in partition:
        continue
      a = {
        '<device>': partition['device'],
        '<number>': partition['number'],
        '<offset>': partition['offset'],
        '<size>':   partition['size'],
        '<name>':   partition['name'],
        '<type>':   partition['type'],
      }
      gpt_create(**a)
##
def gpt_delete(**args):
  if '<device>' in args:
    device = Schema(
      And(Use(os.path.normpath),
          os.path.exists)
    ).validate(args['<device>'])
    if '<number>' in args:
      number = Schema(
        And(Use(int),
            lambda i: i > 0)
      ).validate(args['<number>'])
      call(('/bin/sgdisk',
            '-d', number,
            device))
    else:
      call(('/bin/sgdisk',
            '-Z',
            device))
  else:
    for partition in partitions(**args):
      if 'protected' in partition and partition['protected']:
        continue
      if 'device' not in partition or                                        \
         'number' not in partition:
        continue
      a = {
        '<device>': partition['device'],
        '<number>': partition['number'],
      }
      gpt_delete(**a)
##
def fs_format(**args):
  if '<device>' in args and                                                  \
     '<number>' in args and                                                  \
     '<fs>'     in args:
    device = Schema(
      And(Use(os.path.normpath),
          os.path.exists)
    ).validate(args['<device>'])
    number = Schema(
      And(Use(int),
          lambda i: i > 0)
    ).validate(args['<number>'])
    if os.path.basename(device).startswith('md'):
      device = device + 'p'
    device = '{}{}'.format(device, number)
    fs = Schema(
      Or('bfs',
         'cramfs',
         'exfat',
         'ext2',
         'ext3',
         'ext4',
         'ext4dev',
         'fat32',
         'minix',
         'reiserfs',
         'swap',
         'xfs')
    ).validate(args['<fs>'])
    if fs == 'swap':
      call(('/bin/mkswap',
            device))
    elif fs == 'fat32':
      call(('/bin/mkfs.vfat',
            '-F', '32',
            device))
    else:
      call(('/bin/mkfs.{}'.format(fs),
            device))
  else:
    for partition in partitions(**args):
      if 'protected' in partition and partition['protected']:
        continue
      if 'device' not in partition or                                        \
         'number' not in partition or                                        \
         'fs'     not in partition:
        continue
      a = {
        '<device>': partition['device'],
        '<number>': partition['number'],
        '<fs>':     partition['fs'],
      }
      fs_format(**a)
##
def fs_mount(**args):
  if '<source>' in args and                                                  \
     '<dir>'    in args:
    source = Schema(
      Or(And(Use(os.path.normpath),
             os.path.exists),
         'tmpfs')
    ).validate(args['<source>'])
    dir_path = Schema(
      Use(os.path.normpath)
    ).validate(args['<dir>'])
    mount_args = ['/bin/mount']
    if source == 'tmpfs':
      mount_args.extend(('-t', 'tmpfs'))
    if '--options' in args:
      options = Schema(
        And(str,
            len,
            Use(lambda s: list(map(str, s.split(',')))))
      ).validate(args['--options'])
      options = ','.join(options)
      mount_args.extend(('-o', options))
    mount_args.append(source)
    mount_args.append(dir_path)
    call(('/bin/mkdir',
          '-p',
          dir_path))
    call(mount_args)
  else:
    for partition in partitions(**args):
      if 'fs'  not in partition or                                           \
         'dir' not in partition:
        continue
      if partition['fs'] == 'swap':
        if 'device' not in partition or                                      \
           'number' not in partition:
          continue
        a = {
          '<device>': partition['device'],
          '<number>': partition['number'],
        }
        fs_swap_on(**a)
      elif partition['dir'] == 'none':
        continue
      else:
        a = {
          '<dir>': os.path.join(args['--prefix'],
                                partition['dir'].lstrip('/')),
        }
        if partition['fs'] == 'tmpfs':
          a['<source>'] = 'tmpfs'
        else:
          if 'device' not in partition or                                    \
             'number' not in partition:
            continue
          device = partition['device']
          number = partition['number']
          if os.path.basename(device).startswith('md'):
            device = device + 'p'
          device = '{}{}'.format(device, number)
          a['<source>'] = device
        if 'options' in partition:
          a['--options'] = ','.join(partition['options'])
        fs_mount(**a)
##
def fs_unmount(**args):
  if '<dir>' in args and os.path.isdir(args['<dir>']):
    dir_path = Schema(
      And(Use(os.path.normpath), os.path.isdir)
    ).validate(args['<dir>'])
    call(('/bin/umount',
          dir_path))
  else:
    if '<dir>' in args:
      args['<partitions-file>'] = args.pop('<dir>')
    for partition in reversed(partitions(**args)):
      if 'fs'  not in partition or                                           \
         'dir' not in partition:
        continue
      if partition['fs'] == 'swap':
        if 'device' not in partition or                                      \
           'number' not in partition:
          continue
        a = {
          '<device>': partition['device'],
          '<number>': partition['number'],
        }
        fs_swap_off(**a)
      elif partition['dir'] == 'none':
        continue
      else:
        a = {
          '<dir>': os.path.join(args['--prefix'],
                                partition['dir'].lstrip('/')),
        }
        fs_unmount(**a)
##
def fs_swap_on(**args):
  if '<device>' in args and                                                  \
     '<number>' in args:
    device = Schema(
      And(Use(os.path.normpath),
          os.path.exists)
    ).validate(args['<device>'])
    number = Schema(
      And(Use(int),
          lambda i: i > 0)
    ).validate(args['<number>'])
    if os.path.basename(device).startswith('md'):
      device = device + 'p'
    device = '{}{}'.format(device, number)
    call(('/bin/swapon',
          device))
  else:
    for partition in partitions(**args):
      if 'device' not in partition or                                        \
         'number' not in partition or                                        \
         'fs'     not in partition:
        continue
      if partition['fs'] != 'swap':
        continue
      a = {
        '<device>': partition['device'],
        '<number>': partition['number'],
      }
      fs_swap_on(**a)
##
def fs_swap_off(**args):
  if '<device>' in args and                                                  \
     '<number>' in args:
    device = Schema(
      And(Use(os.path.normpath),
          os.path.exists)
    ).validate(args['<device>'])
    number = Schema(
      And(Use(int),
          lambda i: i > 0)
    ).validate(args['<number>'])
    if os.path.basename(device).startswith('md'):
      device = device + 'p'
    device = '{}{}'.format(device, number)
    call(('/bin/swapoff',
          device))
  else:
    for partition in partitions(**args):
      if 'device' not in partition or                                        \
         'number' not in partition or                                        \
         'fs'     not in partition:
        continue
      if partition['fs'] != 'swap':
        continue
      a = {
        '<device>': partition['device'],
        '<number>': partition['number'],
      }
      fs_swap_off(**a)
##
def fs_table_add(**args):
  if '<source>' in args and                                                  \
     '<dir>'    in args and                                                  \
     '<fs>'     in args and                                                  \
     '<dump>'   in args and                                                  \
     '<pass>'   in args:
    prefix = Schema(
      And(Use(os.path.abspath),
          os.path.isdir)
    ).validate(args['--prefix'])
    options = 'defaults'
    if '--options' in args:
      options = Schema(
        And(str,
            len,
            Use(lambda s: list(map(str, s.split(',')))))
      ).validate(args['--options'])
      options = ','.join(options)
    source = Schema(
      Or(And(Use(os.path.normpath),
             os.path.exists),
         lambda s: s.startswith('PARTLABEL='),
         lambda s: s.startswith(    'LABEL='),
         lambda s: s.startswith('PARTUUID='),
         lambda s: s.startswith(    'UUID='),
         'tmpfs')
    ).validate(args['<source>'])
    dir_path = Schema(
      Or(And(Use(os.path.normpath),
             lambda s: os.path.isdir(os.path.join(prefix, s.lstrip('/')))),
         'none')
    ).validate(args['<dir>'])
    fs = Schema(
      Or('bfs',
         'cramfs',
         'ext2',
         'ext3',
         'ext4',
         'ext4dev',
         'fat32',
         'minix',
         'reiserfs',
         'swap',
         'tmpfs',
         'xfs')
    ).validate(args['<fs>'])
    if fs == 'fat32':
      fs = 'vfat'
    dump = Schema(
      And(Use(int),
          lambda i: i in range(2))
    ).validate(args['<dump>'])
    pass_ = Schema(
      And(Use(int),
          lambda i: i in range(3))
    ).validate(args['<pass>'])
    entry = '{} {} {} {} {} {}\n'.format(source,
                                         dir_path,
                                         fs,
                                         options,
                                         dump,
                                         pass_)
    etc_dir_path = os.path.join(prefix, 'etc')
    call(('/bin/mkdir',
          '-p',
          etc_dir_path))
    fstab_file_path = os.path.join(etc_dir_path, 'fstab')
    if JUST_PRINT:
      just_print("open('{}', 'a+').write('{}')".format(fstab_file_path,
                                                       entry))
    else:
      open(fstab_file_path, 'a+').write(entry)
  else:
    for partition in partitions(**args):
      if 'dir'  not in partition or                                          \
         'fs'   not in partition or                                          \
         'dump' not in partition or                                          \
         'pass' not in partition:
        continue
      a = {
        '--prefix':  args['--prefix'],
        '<dir>':     partition['dir'],
        '<fs>':      partition['fs'],
        '<dump>':    partition['dump'],
        '<pass>':    partition['pass'],
      }
      if partition['fs'] == 'tmpfs':
        a['<source>'] = 'tmpfs'
      else:
        if 'device' not in partition or                                      \
           'number' not in partition:
          continue
        device = partition['device']
        number = partition['number']
        if os.path.basename(device).startswith('md'):
          device = device + 'p'
        device = '{}{}'.format(device, number)
        if '--label' in args and args['--label']:
          try:
            s = blkid('PARTLABEL', device)
          except:
            s = blkid(    'LABEL', device)
        if '--uuid' in args and args['--uuid']:
          try:
            s = blkid('PARTUUID', device)
          except:
            s = blkid(    'UUID', device)
        if 's' in locals():
          from re import match
          from re import sub
          m = match(r'\b(?P<tag>\w+)="(?P<value>.*)"', s)
          a['<source>'] = '{}={}'.format(m.group('tag'),
                                         sub(r' ', '\\040', m.group('value')))
        else:
          a['<source>'] = device
      if 'options' in partition:
        a['--options'] = ','.join(partition['options'])
      fs_table_add(**a)
##
# ----------------------------------------------------------------------------
# CAUTION:
#
# For BIOS Boot Partition (BBP) installations only!
#
def grub_bios_install(**args):
  prefix = Schema(
    And(Use(os.path.abspath),
        os.path.isdir)
  ).validate(args['--prefix'])
  device = Schema(
    And(Use(os.path.normpath),
        os.path.exists)
  ).validate(args['<device>'])
  grub_cfg_file     = '/boot/grub/grub.cfg'
  grub_cfg_new_file = grub_cfg_file + '.new'
  grub_cfg_old_file = grub_cfg_file + '.old'
  require_not_exists(grub_cfg_new_file)
  require_not_exists(grub_cfg_old_file)
  if os.path.exists(grub_cfg_file):
    call(('/bin/mv',
          grub_cfg_file,
          grub_cfg_old_file))
  require_not_exists(grub_cfg_file)
  check_grub_install()
  check_grub_mkconfig()
  check_intel_ucode()
  check_os_prober()
  sh_args = ['/bin/sh', '-c',
             ' && '.join((' '.join(('grub-install',
                                    '--target=i386-pc',
                                    '--recheck',
                                    device)),
                          ' '.join(('grub-mkconfig',
                                    '-o',
                                    grub_cfg_file))))]
  if prefix != '/':
    sh_args[:0] = ('arch-chroot', prefix)
  call(sh_args)
  require_not_exists(grub_cfg_new_file)
# ----------------------------------------------------------------------------
##
# ----------------------------------------------------------------------------
# CAUTION:
#
# For EFI System Partition (ESP) installations only!
#
def grub_uefi_install(**args):
  prefix = Schema(
    And(Use(os.path.abspath),
        os.path.isdir)
  ).validate(args['--prefix'])
  boot_dir = '/boot'
  if '--esp' in args and args['--esp']:
    boot_dir = '/boot/efi'
  target = 'x86_64-efi'
  suffix = 'x64'
  if '--i386' in args and args['--i386']:
    target = 'i386-efi'
    suffix = 'ia32'
  grub_cfg_file     = os.path.join(boot_dir, 'grub/grub.cfg')
  grub_cfg_new_file = grub_cfg_file + '.new'
  grub_cfg_old_file = grub_cfg_file + '.old'
  require_not_exists(grub_cfg_new_file)
  require_not_exists(grub_cfg_old_file)
  if os.path.exists(grub_cfg_file):
    call(('/bin/mv',
          grub_cfg_file,
          grub_cfg_old_file))
  require_not_exists(grub_cfg_file)
  check_grub_install()
  check_grub_mkconfig()
  check_intel_ucode()
  check_os_prober()
  sh_args = ['/bin/sh', '-c',
             ' && '.join((' '.join(('pacman',
                                    '--sync',
                                    '--noconfirm',
                                    'efibootmgr')),
                          ' '.join(('grub-install',
                                    '--target={}'.format(target),
                                    '--boot-directory={}'.format(boot_dir),
                                    '--efi-directory=/boot/efi',
                                    '--bootloader-id=GRUB',
                                    '--recheck')),
                          ' '.join(('grub-mkconfig',
                                    '-o',
                                    grub_cfg_file))))]
  if prefix != '/':
    sh_args[:0] = ('arch-chroot', prefix)
  call(sh_args)
  require_not_exists(grub_cfg_new_file)
  if '--default' in args and args['--default']:
    efi_grub_dir  = os.path.join(prefix, 'boot/efi/EFI/GRUB')
    efi_boot_dir  = os.path.join(prefix, 'boot/efi/EFI/boot')
    grub_efi_file = os.path.join(efi_grub_dir, 'grub{}.efi'.format(suffix))
    boot_efi_file = os.path.join(efi_boot_dir, 'boot{}.efi'.format(suffix))
    require_not_exists(boot_efi_file)
    call(('/bin/mkdir',
          '-p',
          efi_boot_dir))
    call(('/bin/cp',
          grub_efi_file,
          boot_efi_file))
# ----------------------------------------------------------------------------
##
def backup(**args):
  prefix = Schema(
    And(Use(os.path.abspath),
        os.path.isdir)
  ).validate(args['--prefix'])
  compressor_path = None
  if '--compressor' in args:
    compressor_path = Schema(
      Or(Use(os.path.normpath), '')
    ).validate(args['--compressor'])
  extension = None
  if compressor_path:
    extension = {
      'bzip2':  'bz2',
      'pbzip2': 'bz2',
      'gzip':   'gz',
      'pigz':   'gz',
      'lzip':   'lz',
      'plzip':  'lz',
      'xz':     'xz',
      'pxz':    'xz',
      'lrzip':  'lrz',
      'lzma':   'lzma',
      'lzop':   'lzo',
      'zip':    'zip',
    }.get(os.path.basename(compressor_path))
  backup_dir_path = Schema(
    Use(os.path.normpath)
  ).validate(args['--backup-dir'])
  verbose = Schema(
    Use(bool)
  ).validate(args['--verbose'])
  call(('/bin/mkdir',
        '-p',
        backup_dir_path))
  bsdtar_args = ['/bin/bsdtar']
  bsdtar_args.append('-c')
  ##
  def backup_file_path(backup_dir_path, extension=None):
    from datetime import date
    from platform import node
    node     = node()
    date     = date.today().strftime('%d.%m.%Y')
    index    = 1
    template = '{}-{}-{}.backup.tar'
    if extension:
      template = '.'.join((template, extension))
    backup_file_name = template.format(node, date, index)
    backup_file_path = os.path.join(backup_dir_path, backup_file_name)
    while os.path.isfile(backup_file_path):
      index = index + 1
      backup_file_name = template.format(node, date, index)
      backup_file_path = os.path.join(backup_dir_path, backup_file_name)
    return backup_file_path
  ##
  file_path = backup_file_path(backup_dir_path, extension)
  bsdtar_args.extend(('-f', file_path))
  if verbose:
    bsdtar_args.append('-v')
  bsdtar_args.append('-l')
  bsdtar_args.extend(('-C', prefix))
  if '--excludes-file' in args:
    excludes_file = Schema(
      Use(open_excludes_file)
    ).validate(args['--excludes-file'])
    ##
    def excludes(excludes_file):
      from json import load
      return load(excludes_file)
    ##
    for exclude in excludes(excludes_file):
      bsdtar_args.extend(('--exclude', exclude))
  if compressor_path:
    bsdtar_args.append('--use-compress-program={}'.format(compressor_path))
  bsdtar_args.extend(('-s', '/^\.//'))
  bsdtar_args.append('.')
  for e in {'BZIP',
            'BZIP2',
            'GZIP',
            'GZIP_OPT',
            'LZIP',
            'LZOP',
            'XZ_OPT',
            'ZIPOPT'}:
    if not e in os.environ:
      os.environ[e] = '-9'
  from datetime import timedelta
  from time     import time
  start = time()
  call(bsdtar_args, env=os.environ)
  elapsed = time() - start
  message("Elapsed time:", timedelta(seconds=elapsed))
##
def deploy(**args):
  prefix = Schema(
    And(Use(os.path.abspath),
        os.path.isdir)
  ).validate(args['--prefix'])
  decompressor_path = None
  if '--decompressor' in args:
    decompressor_path = Schema(
      Or(Use(os.path.normpath), '')
    ).validate(args['--decompressor'])
  verbose = Schema(
    Use(bool)
  ).validate(args['--verbose'])
  backup_file_path = Schema(
    And(Use(os.path.normpath),
        os.path.isfile)
  ).validate(args['<backup-file>'])
  bsdtar_args = ['/bin/bsdtar']
  bsdtar_args.append('-x')
  bsdtar_args.extend(('-f', backup_file_path))
  if verbose:
    bsdtar_args.append('-v')
  bsdtar_args.append('-p')
  bsdtar_args.extend(('-C', prefix))
  if decompressor_path:
    bsdtar_args.append('--use-compress-program={}'.format(decompressor_path))
  from datetime import timedelta
  from time     import time
  start = time()
  call(bsdtar_args)
  elapsed = time() - start
  message("Elapsed time:", timedelta(seconds=elapsed))
##  ==========================================================================
##  }}} Private
##  ==========================================================================
##  }}} Functions
##
### Classes {{{
##  ==========================================================================
### Public {{{
##  ==========================================================================
##  ==========================================================================
##  }}} Public
##
### Private {{{
##  ==========================================================================
class PermissionError(Exception):
  def __init__(self, message):
    Exception.__init__(self, message)
##  ==========================================================================
##  }}} Private
##  ==========================================================================
##  }}} Classes
##
### Main {{{
##  ==========================================================================
if __name__ == '__main__':
  sys.exit(main())
##  ==========================================================================
##  }}} Main
