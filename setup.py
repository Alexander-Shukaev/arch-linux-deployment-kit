#!/usr/bin/env python
# encoding: utf-8
##
### Preamble {{{
##  ==========================================================================
##        @file setup.py
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-10-07 Sunday 01:37:22 (+0200)
##  --------------------------------------------------------------------------
##     @created 2016-01-05 Tuesday 22:02:47 (+0100)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Imports {{{
##  ==========================================================================
from __future__ import absolute_import
from __future__ import print_function
##
import os
import sys
##  ==========================================================================
##  }}} Imports
##
### Constants {{{
##  ==========================================================================
__realfile__ = os.path.realpath(__file__)
__dir__      = os.path.dirname(__file__)
__realdir__  = os.path.dirname(__realfile__)
##  ==========================================================================
##  }}} Constants
##
### Classes {{{
##  ==========================================================================
class path():
  def __init__(self, path):
    self.path = path
  ##
  def __enter__(self):
    sys.path.insert(0, self.path)
  ##
  def __exit__(self, exc_type, exc_value, traceback):
    sys.path.remove(self.path)
##  ==========================================================================
##  }}} Classes
##
### Imports {{{
##  ==========================================================================
with path(os.path.join('share', 'make', 'scripts', 'python')):
  import setup
##  ==========================================================================
##  }}} Imports
##
### Functions {{{
##  ==========================================================================
##  ==========================================================================
##  }}} Functions
##
### Setup {{{
##  ==========================================================================
### Constants {{{
##  ==========================================================================
NAME                 = 'aldk'
DESCRIPTION          = 'Arch Linux Deployment Kit (ALDK).'
LONG_DESCRIPTION     = setup.parse_long_description()
KEYWORDS             = [
  'Arch Linux',
  'GRUB',
  'Linux',
  'archiving',
  'automation',
  'backup',
  'boot',
  'compression',
  'deployment',
  'filesystem',
  'installation',
  'migration',
  'partitioning',
  'tool',
]
AUTHOR               = None
AUTHOR_EMAIL         = None
MAINTAINER           = AUTHOR
MAINTAINER_EMAIL     = AUTHOR_EMAIL
URL                  = 'http://Alexander.Shukaev.name'
LICENSE              = 'GPLv3+'
CLASSIFIERS          = [
  'Development Status :: 5 - Production/Stable',
  'Intended Audience :: End Users/Desktop',
  'Intended Audience :: Developers',
  'License :: OSI Approved :: '
  'GNU General Public License v3 or later (GPLv3+)',
  'Operating System :: POSIX :: Linux',
  'Topic :: System :: Archiving :: Backup',
  'Topic :: System :: Archiving :: Compression',
  'Topic :: System :: Boot',
  'Topic :: System :: Filesystems',
  'Topic :: System :: Installation/Setup',
  'Topic :: System :: Operating System Kernels :: Linux',
  'Topic :: System :: Recovery Tools',
  'Topic :: System :: Systems Administration',
  'Topic :: Utilities',
  'Programming Language :: Python',
  'Programming Language :: Python :: 3',
  'Programming Language :: Python :: Implementation :: CPython',
  'Programming Language :: Python :: Implementation :: PyPy',
]
PLATFORMS            = [
  'linux',
]
PACKAGE_DIR          = os.path.join('source')
PACKAGES             = (setup.find_packages(PACKAGE_DIR, include=[NAME])
                        if NAME else [])
INCLUDE_PACKAGE_DATA = True
PY_MODULES           = setup.find_py_modules(PACKAGE_DIR)
DATA_DIR             = (os.path.join('share', NAME)
                        if NAME else None)
DATA_FILES           = setup.find_data_files(DATA_DIR)
SCRIPTS              = [
  'bin/aldk-prepare',
]
ENTRY_POINTS         = {
  'console_scripts': [
    'aldk=aldk:main',
  ],
}
PYTHON_REQUIRES      = '>=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*'
SETUP_REQUIRES       = [
  'setuptools-scm',
] + ([
  'pytest-runner',
] if {'test', 'pytest', 'ptr'}.intersection(sys.argv) else [])
INSTALL_REQUIRES     = setup.check_requirements()
TESTS_REQUIRE        = [
  'pytest',
  'pytest-timeout',
]
EXTRAS_REQUIRE       = {
  'test': TESTS_REQUIRE,
}
ZIP_SAFE             = False
##  ==========================================================================
##  }}} Constants
##
### Attributes {{{
##  ==========================================================================
attrs = dict(
  name=NAME,
  use_scm_version={
    'write_to':       (os.path.join(PACKAGE_DIR, NAME, '__version.py') if NAME
                       else os.path.join(PACKAGE_DIR, '__version.py')),
    'version_scheme': 'python-simplified-semver',
  },
  description=DESCRIPTION,
  long_description=LONG_DESCRIPTION,
  keywords=KEYWORDS,
  author=AUTHOR,
  author_email=AUTHOR_EMAIL,
  maintainer=MAINTAINER,
  maintainer_email=MAINTAINER_EMAIL,
  url=URL,
  license=LICENSE,
  classifiers=CLASSIFIERS,
  platforms=PLATFORMS,
  package_dir={'': PACKAGE_DIR},
  packages=PACKAGES,
  include_package_data=INCLUDE_PACKAGE_DATA,
  py_modules=PY_MODULES,
  data_files=DATA_FILES,
  scripts=SCRIPTS,
  entry_points=ENTRY_POINTS,
  python_requires=PYTHON_REQUIRES,
  setup_requires=SETUP_REQUIRES,
  install_requires=INSTALL_REQUIRES,
  tests_require=TESTS_REQUIRE,
  extras_require=EXTRAS_REQUIRE,
  zip_safe=ZIP_SAFE,
)
##  ==========================================================================
##  }}} Attributes
##  ==========================================================================
##  }}} Setup
##
### Main {{{
##  ==========================================================================
if __name__ == '__main__':
  sys.exit(setup.main(**attrs))
##  ==========================================================================
##  }}} Main
